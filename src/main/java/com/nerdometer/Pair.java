package com.nerdometer;

class Pair<T,S>{
    private T key;
    private S val;

    Pair(T key, S val) {
        this.key = key;
        this.val = val;
    }

    T getKey() {
        return key;
    }

    public void setKey(T key) {
        this.key = key;
    }

    S getVal() {
        return val;
    }

    public void setVal(S val) {
        this.val = val;
    }
}
