package com.nerdometer;


import com.nerdometer.model.NerdActivityLog;
import com.nerdometer.model.NerdActivityRating;
import com.nerdometer.services.DatabaseService;
import com.nerdometer.services.DatabaseServiceMongo;
import spark.Request;


import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

import static spark.Spark.*;
import static com.nerdometer.model.NerdActivityRating.*;

public class Main {
    private static final String DATE_FROM = "date-from";
    private static final String DATE_TO = "date-to";
    private static final java.lang.String PAIR_SEPARATOR = ";";
    private static final java.lang.String PAIR_ITEM_SEPARATOR = ",";

    private static final DatabaseService databaseService = new DatabaseServiceMongo();



    public static void main(String[] args) {
        post("/pc_activity", (req, res) -> postUpdatePcActivity());

        post("/steps_activity", (req, res) -> postUpdateStepsActivity(req));

        post("/room_activity", (req, res) -> postUpdateRoomActivity(req));

        get("/nerd_activity_log", (req, res) -> getActivityLogForTime(req));
    }

    private static Object postUpdateRoomActivity(Request req) {
        int minutesInRoom = Integer.valueOf(req.queryParams("body"));
        databaseService.updateRoomActivity(minutesInRoom, LocalDate.now());
        return "room_activity updated";
    }

    private static Object getActivityLogForTime(Request req) {
        LocalDate from = LocalDate.parse(req.headers(DATE_FROM));
        LocalDate to = LocalDate.parse(req.headers(DATE_TO));
        List<NerdActivityLog> logs = acquireNerdActivityLogs(from, to);
        StringBuffer result = new StringBuffer();

        int avgPc = (int)logs.stream().mapToInt(NerdActivityLog::getPcActivityCounter).filter(x -> x != 0).average().orElse(-1);
        int avgMov = (int)logs.stream().mapToInt(NerdActivityLog::getMinutesInRoom).filter(x -> x != 0).average().orElse(-1);
        int avgSte = (int)logs.stream().mapToInt(NerdActivityLog::getStepsCounter).filter(x -> x != 0).average().orElse(-1);

        NerdActivityLog periodLog = new NerdActivityLog(avgPc, avgMov, avgSte);
        NERD_LEVEL periodPcRating = NerdActivityRating.calculateForActivity(avgPc, pcActivityLevels);
        NERD_LEVEL periodMovementRating = NerdActivityRating.calculateForActivity(avgMov, movementActivityLevels);
        NERD_LEVEL periodStepsRating = NerdActivityRating.calculateForActivity(avgSte, stepsActivityLevels);
        NERD_LEVEL periodOverallRating = NerdActivityRating.calculateOverallRating(periodLog);
        result.append(periodPcRating.ordinal()).append(",").append(periodMovementRating.ordinal()).
                append(",").append(periodStepsRating.ordinal()).append(",").append(periodOverallRating.ordinal());
        return result;
    }

    private static Object postUpdateStepsActivity(Request req) {
        String responseString = req.body();
        List<String> rawDateStepsList = Arrays.asList(responseString.split(PAIR_SEPARATOR));
        List<Pair<LocalDate, Integer>> dateStepsPairs = rawDateStepsList.stream().
                map(tmp -> {
                    String [] dateSteps = tmp.split(PAIR_ITEM_SEPARATOR);
                    return new Pair<>(LocalDate.parse(dateSteps[0]), Integer.valueOf(dateSteps[1]));
                }).collect(Collectors.toList());
        dateStepsPairs.forEach(x -> databaseService.updateStepsActivity(x.getKey(), x.getVal()));
        System.out.println("Steps updated");
        return "steps_activity updated";
    }

    private static Object postUpdatePcActivity() {
        databaseService.updatePcActivity(LocalDate.now());
        return "pc_activity updated";
    }

    private static List<NerdActivityLog> acquireNerdActivityLogs(LocalDate from, LocalDate to) {
        List<NerdActivityLog> resultLogs = new LinkedList<>();
        LocalDate current = LocalDate.of(from.getYear(), from.getMonthValue(), from.getDayOfMonth());
        do {
            Optional<NerdActivityLog> log = databaseService.getNerdActivityLog(current);
            if (log.isPresent())
                resultLogs.add(log.get());
            current = current.plusDays(1);
        } while (!current.isAfter(to));
        return resultLogs;
    }
}
