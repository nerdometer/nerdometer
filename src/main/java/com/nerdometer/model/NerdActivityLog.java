package com.nerdometer.model;

import org.mongodb.morphia.annotations.Embedded;
import org.mongodb.morphia.annotations.Entity;
import org.mongodb.morphia.annotations.Id;
import org.mongodb.morphia.annotations.Property;

import java.time.LocalDate;

@Entity
public class NerdActivityLog {
    @Id
    private long id;
    @Property
    private
    int pcActivityCounter;
    @Property
    private
    int stepsCounter;
    @Property
    private
    int minutesInRoom;
    @Embedded
    private LocalDate logDate;

    public NerdActivityLog(int pcActivityCounter, int stepsCounter, int minutesInRoom) {
        this.pcActivityCounter = pcActivityCounter;
        this.stepsCounter = stepsCounter;
        this.minutesInRoom = minutesInRoom;
    }

    public NerdActivityLog(LocalDate logDate){
        pcActivityCounter = 0;
        stepsCounter = 0;
        this.logDate = logDate;
        this.id = logDate.hashCode();
    }

    public NerdActivityLog() {
    }

    @Override
    public String toString() {
        return logDate + "," + pcActivityCounter + "" + stepsCounter;
    }

    public int getPcActivityCounter() {
        return pcActivityCounter;
    }

    public void setPcActivityCounter(int pcActivityCounter) {
        this.pcActivityCounter = pcActivityCounter;
    }

    public int getStepsCounter() {
        return stepsCounter;
    }

    public void setStepsCounter(int stepsCounter) {
        this.stepsCounter = stepsCounter;
    }

    public int getMinutesInRoom() {
        return minutesInRoom;
    }

    public void setMinutesInRoom(int minutesInRoom) {
        this.minutesInRoom = minutesInRoom;
    }

    public LocalDate getLogDate() {
        return logDate;
    }

    public void setLogDate(LocalDate logDate) {
        this.logDate = logDate;
        this.id = logDate.hashCode();
    }

    public long getId() {
        return id;
    }
}
