package com.nerdometer.model;

import java.util.Arrays;
import java.util.List;

public class NerdActivityRating {
    public static final List<Integer> pcActivityLevels = Arrays.asList(
            50, 40, 30, 20, -1
    );

    public static final List<Integer> stepsActivityLevels = Arrays.asList(
            50, 40, 30, 20, -1
    );

    public static final List<Integer> movementActivityLevels = Arrays.asList(
            50, 40, 30, 20, -1
    );

    public enum NERD_LEVEL {
        KING_OF_NOLIFE,
        LOCAL_NERD_MASTER,
        REGULAR_NERD,
        A_BIT_NERDY,
        NORMIE,
        UNKNOWN
    }

    public enum NERD_FACTOR{
        PC_ACTIVITY,
        STEPS_ACTIVITY,
        ROOM_ACTIVITY
    }

    public static NERD_LEVEL calculateForActivity(int activity, List<Integer> levelList){

        for(int i = 0; i < levelList.size(); ++i){
            if(activity > levelList.get(i))
                return NERD_LEVEL.values()[i];
        }
        return NERD_LEVEL.UNKNOWN;
    }

    public static NERD_LEVEL calculateOverallRating(NerdActivityLog nerdLog){
        NERD_LEVEL [] nerdFactors = {calculateForActivity(nerdLog.getPcActivityCounter(), pcActivityLevels),
            calculateForActivity(nerdLog.getStepsCounter(), stepsActivityLevels),
                calculateForActivity(nerdLog.getMinutesInRoom(), movementActivityLevels)};
        double dailyLevelOrdinal = Arrays.stream(nerdFactors).filter(x -> !x.equals(NERD_LEVEL.UNKNOWN)).
                mapToInt(Enum::ordinal).average().orElse(5);
        return NERD_LEVEL.values()[(int)dailyLevelOrdinal];
    }
}
