package com.nerdometer.services;

import com.nerdometer.model.NerdActivityLog;

import java.time.LocalDate;
import java.util.Optional;

public interface DatabaseService {
    Optional<NerdActivityLog> getNerdActivityLog(LocalDate date);
    void updatePcActivity(LocalDate date);
    void updateStepsActivity(LocalDate now, int numOfSteps);
    void updateRoomActivity(int minutesInRoom, LocalDate date);
}
