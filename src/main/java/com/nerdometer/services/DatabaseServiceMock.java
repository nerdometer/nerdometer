package com.nerdometer.services;

import com.nerdometer.model.NerdActivityLog;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class DatabaseServiceMock implements DatabaseService{
    private final Map<LocalDate, NerdActivityLog> activityDatabase = new HashMap<>();

    public Optional<NerdActivityLog> getNerdActivityLog(LocalDate date){
        return Optional.ofNullable(activityDatabase.get(date));
    }

    public void updatePcActivity(LocalDate date) {
        NerdActivityLog nerdActivityLog = activityDatabase.get(date);
        if(nerdActivityLog == null){
            nerdActivityLog = new NerdActivityLog(date);
            activityDatabase.put(date, nerdActivityLog);
        }
        nerdActivityLog.setPcActivityCounter(nerdActivityLog.getPcActivityCounter() + 1);
    }

    public void updateStepsActivity(LocalDate date, int numOfSteps) {
        NerdActivityLog nerdActivityLog = activityDatabase.get(date);
        if(nerdActivityLog == null){
            nerdActivityLog = new NerdActivityLog(date);
            activityDatabase.put(date, nerdActivityLog);
        }
        nerdActivityLog.setStepsCounter(numOfSteps);
    }

    @Override
    public void updateRoomActivity(int minutesInRoom, LocalDate date) {
        NerdActivityLog nerdActivityLog = activityDatabase.get(date);
        if(nerdActivityLog == null){
            nerdActivityLog = new NerdActivityLog(date);
            activityDatabase.put(date, nerdActivityLog);
        }
        nerdActivityLog.setMinutesInRoom(minutesInRoom);
    }
}
