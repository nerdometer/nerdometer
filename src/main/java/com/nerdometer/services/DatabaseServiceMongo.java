package com.nerdometer.services;

import com.mongodb.MongoClient;
import com.nerdometer.model.NerdActivityLog;
import org.mongodb.morphia.Datastore;
import org.mongodb.morphia.Morphia;

import java.time.LocalDate;
import java.util.Optional;

public class DatabaseServiceMongo implements DatabaseService {
    private final Morphia morphia = new Morphia();
    private final Datastore datastore = morphia.createDatastore(new MongoClient(), "NerdometerDB");

    @Override
    public Optional<NerdActivityLog> getNerdActivityLog(LocalDate date) {
        return Optional.ofNullable(datastore.createQuery(NerdActivityLog.class).field("logDate").equal(date).get());
    }

    @Override
    public void updatePcActivity(LocalDate date) {
        NerdActivityLog nerdActivityLog = getNerdActivityLog(date).orElse(new NerdActivityLog(date));
        nerdActivityLog.setPcActivityCounter(nerdActivityLog.getPcActivityCounter() + 1);
        datastore.save(nerdActivityLog);
    }

    @Override
    public void updateStepsActivity(LocalDate date, int numOfSteps) {
        NerdActivityLog nerdActivityLog = getNerdActivityLog(date).orElse(new NerdActivityLog(date));
        nerdActivityLog.setStepsCounter(numOfSteps);
        datastore.save(nerdActivityLog);
    }

    @Override
    public void updateRoomActivity(int minutesInRoom, LocalDate date) {
        NerdActivityLog nerdActivityLog = getNerdActivityLog(date).orElse(new NerdActivityLog(date));
        nerdActivityLog.setMinutesInRoom(nerdActivityLog.getMinutesInRoom() + minutesInRoom);
        datastore.save(nerdActivityLog);
    }
}
